import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type RestauranteModelDocument = HydratedDocument<RestauranteModel>;

@Schema()
export class RestauranteModel {
  static collectionName = 'PRUTR_RESTAURANT';
  @Prop()
  sNombre: string;
  
  @Prop()
  sDescripcion:string;

  @Prop()
  sTelefeno: string;

  @Prop()
  lTipo: string[];
}

export const RestauranteSchema = SchemaFactory.createForClass(RestauranteModel);