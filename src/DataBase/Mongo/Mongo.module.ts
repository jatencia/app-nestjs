// import { Global, Module } from "@nestjs/common";
// import { MongooseModule } from "@nestjs/mongoose";
// import { RestauranteModel, RestauranteSchema } from "./Model/restaurante.scheme";
// import { RestauranteRepository } from "./Repositorio/restaurante.repository";



// @Global()
// @Module({
//     imports: [
//         MongooseModule.forRootAsync({
//             useFactory: () => ({
//               uri: process.env.MONGO_URI,
//             }),
//           }),

//         MongooseModule.forFeature([
//             {
//                 name: RestauranteModel.name,
//                 schema: RestauranteSchema
//             }
//         ])
//     ],
//     providers: [RestauranteRepository],
//     exports: [
//         MongooseModule,
//         RestauranteRepository
//     ],
// })


// export class MongoModule {}