import { Module } from '@nestjs/common';
import { ClienteService } from './cliente.service';
import { ClienteController } from './cliente.controller';
import { RestauranteService } from '../restaurante/restaurante.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RestauranteModel, RestauranteSchema } from 'src/DataBase/Mongo/Model/restaurante.scheme';

@Module({
  imports:[
    MongooseModule.forFeature([
      {
          name: RestauranteModel.collectionName,
          schema: RestauranteSchema
      }
  ])
  ],
  controllers: [ClienteController],
  providers: [ClienteService, RestauranteService]
})
export class ClienteModule {}
