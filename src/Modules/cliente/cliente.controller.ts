import { Body, Controller, Get, Query } from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { TypeFoodEnum } from 'src/Modules/restaurante/dto/create-restaurante.dto';
import { RestauranteService } from '../restaurante/restaurante.service';
import { ClienteService } from './cliente.service';

@Controller('cliente')
@ApiTags('Restaurante')
export class ClienteController {
  constructor(private readonly clienteService: ClienteService,
    private readonly restauranteService: RestauranteService) {}

  @Get('Listar-Restaurantes')
  @ApiQuery({ name: 'TypeFood', enum: TypeFoodEnum, isArray: true })
  indAll(@Query('TypeFood') TypeFood: TypeFoodEnum[]) {
    return this.restauranteService.findOne(TypeFood);
  }

}
