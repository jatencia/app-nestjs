import { Module } from '@nestjs/common';
import { RestauranteService } from './restaurante.service';
import { RestauranteController } from './restaurante.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { RestauranteModel, RestauranteSchema } from 'src/DataBase/Mongo/Model/restaurante.scheme';

@Module({
  imports:[
    MongooseModule.forFeature([
      {
          name: RestauranteModel.collectionName,
          schema: RestauranteSchema
      }
  ])
  ],
  controllers: [RestauranteController],
  providers: [RestauranteService]
})
export class RestauranteModule {}
