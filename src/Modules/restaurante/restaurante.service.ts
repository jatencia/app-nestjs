import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RestauranteModel, RestauranteModelDocument } from 'src/DataBase/Mongo/Model/restaurante.scheme';
import { CreateRestauranteDto } from './dto/create-restaurante.dto';
import { UpdateRestauranteDto } from './dto/update-restaurante.dto';

@Injectable()
export class RestauranteService {

  constructor(@InjectModel(RestauranteModel.collectionName)
  private RestauranteModel: Model<RestauranteModelDocument>){

  }
  create(createRestauranteDto: CreateRestauranteDto) 
  {
    const record:RestauranteModel = {
      sNombre: createRestauranteDto.Nombre,
      sDescripcion: createRestauranteDto.Descripcion,
      sTelefeno: createRestauranteDto.Telefeno,
      lTipo: createRestauranteDto.Tipo
    }
    const result = this.RestauranteModel.create(record);
    return result;
  }

  findAll() {
    return this.RestauranteModel.find({}).exec();
  }

  async findOne(records: string[]) {
    if (typeof records === "string" ){
      records = [records];
    }
    let ob = [];
    records.forEach(async element => {
        ob.push({'lTipo': { $all: [element] }})
      });
    return  this.RestauranteModel.find({}).or(ob).exec();
  }

  update(id: number, updateRestauranteDto: UpdateRestauranteDto) {
    return `This action updates a #${id} restaurante`;
  }

  remove(id: number) {
    return `This action removes a #${id} restaurante`;
  }
}
