import { IsArray, IsNotEmpty, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateRestauranteDto {
    @IsNotEmpty()
    @Length(1, 120)
    @ApiProperty()
    Nombre:string;

    @IsNotEmpty()
    @Length(1, 520)
    @ApiProperty()
    Descripcion:string;

    @IsNotEmpty()
    @Length(1, 20)
    @ApiProperty()
    Telefeno:string;

    @ApiProperty()
    @IsArray()
    Tipo:TypeFoodEnum[];

}


export enum TypeFoodEnum {
    Pastas = 'Pastas',
    Pizza = 'Pizza',
    Sopas = 'Sopas',
    SalchiPapas = 'SalchiPapas',
    Arroces = 'Arroces',
    Postres = 'Postres',
    Horneados = 'Horneados',
    Fritos = 'Fritos',
    PerrosCalientes = 'PerrosCalientes',
    Hamburguesas = 'Hamburguesas'
  }
  