import { Module, ValidationPipe } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RestauranteModule } from './Modules/restaurante/restaurante.module';
import { AuthModule } from './Modules/auth/auth.module';
import { ClienteModule } from './Modules/cliente/cliente.module';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';


@Module({
  imports: [
    ConfigModule.forRoot(),
    RestauranteModule, 
    AuthModule, 
    ClienteModule,
    //MongoModule,
    MongooseModule.forRoot(process.env.MONGO_URI)
  ],
  controllers: [AppController],
  providers: [AppService
  ],
})
export class AppModule {}
